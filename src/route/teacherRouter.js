import { Router } from "express";
import { createTeacher, deleteTeacher, readSpecificTeacher, readTeacher, updatedTeacher } from "../controller/teacherController.js";

export let teacherRouter = Router()

teacherRouter
.route("/")
.post(createTeacher)
.get(readTeacher)

teacherRouter
.route("/:id")
.get(readSpecificTeacher)
.patch(updatedTeacher)
.delete(deleteTeacher)
