import { Router } from "express";
import { createCollege, deleteCollege, readCollege, readSpecificCollege, updateCollege } from "../controller/collegeController.js";

export let collegeRouter= Router()

collegeRouter
.route("/")
.post(createCollege)
.get(readCollege)


collegeRouter
.route("/:id")
.get(readSpecificCollege)
.patch(updateCollege)
.delete(deleteCollege)