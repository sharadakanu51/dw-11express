// import express from "express"
import express, { json } from "express"
import { firstRouter } from "./src/route/firstRouter.js"
import { traineesRouter } from "./src/route/traineesRouter.js"
import { schoolRouter,  } from "./src/route/schoolRouter.js"
import { vehiclesRouter } from "./src/route/vehiclesRouter.js"
import { hospitalRouter } from "./src/route/hospitalRouter.js"
import { connectToMongoDb } from "./src/connectTODb/connectTOMongoDb.js"
import { traineesRouter2 } from "./src/route/traineesRouter2.js"
import { studentSchema } from "./src/schema/studentSchema.js"
import { studentRouter } from "./src/route/studentRouter.js"
import { descriptionRouter } from "./src/route/descriptionRouter.js"
import { contactRouter } from "./src/route/contactRouter.js"
import { classRoomRouter } from "./src/route/classRoomRouter.js"
import { departmentRouter } from "./src/route/departmentRouter.js"
import { hostelRouter } from "./src/route/hostelRouter.js"
import { collegeRouter } from "./src/route/collegeRouter.js"
import { teacherRouter } from "./src/route/teacherRouter.js"
import { bookRouter } from "./src/route/bookRouter.js"
import { productRouter } from "./src/route/productRouter.js"
import { userRouter } from "./src/route/userRouter.js"
import { reviewRouter } from "./src/route/reviewRouter.js"
import bcrypt from "bcrypt"
import  jwt  from "jsonwebtoken"
import { config } from "dotenv"
import { port, secretKey } from "./src/constant.js"
config()




let expressApp=express()
expressApp.use(json())
connectToMongoDb()
// expressApp.use((req,res,next)=>{
//     console.log("i am application  middleware")
//     next()
// })
expressApp.use("/bike",firstRouter)
expressApp.use("/trainees",traineesRouter)
expressApp.use("/school",schoolRouter)
expressApp.use("/vehicles",vehiclesRouter)
expressApp.use("/hospital",hospitalRouter)
expressApp.use("/trainees2",traineesRouter2)
expressApp.use("/students",studentRouter)
expressApp.use("/descriptions",descriptionRouter)
expressApp.use("/contacts",contactRouter)
expressApp.use("/classRooms",classRoomRouter)
expressApp.use("/departments",departmentRouter)
expressApp.use("/hostels",hostelRouter)
expressApp.use("/colleges",collegeRouter)
expressApp.use("/teachers",teacherRouter)
expressApp.use("/books",bookRouter)
expressApp.use("/products",productRouter)
expressApp.use("/users",userRouter)
expressApp.use("/reviews",reviewRouter)


expressApp.listen(port,()=>{
    console.log("app listening at port 8000")

})




//make backend application (using express)
// attached port to that application.

// hash

// let password = "abc@123"
// let hashPassword = await bcrypt.hash(password,10)
// console.log(hashPassword)

//compare hash

// let hashPassword ="$2b$10$Q1VC2/olKBMcWAfFsAto4.QklPoN3JKEMk2WlaUfyBwsIO0ZrZksC"

// let password = "abc@123"
// let isPasswordMatch = await bcrypt.compare(password, hashPassword)
// console.log(isPasswordMatch) // output true false ma aaunx


//generate token  (just like idcard banaune)

let infoObj ={
_id: "123589",
}


let expiryInfo={
    expiresIn:"365d"
}
let token = jwt.sign(infoObj,secretKey,expiryInfo)
console.log(token)

//validate


// let token ="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiIxMjM1ODkiLCJpYXQiOjE3MDYzNDMyMDUsImV4cCI6MTczNzg3OTIwNX0.QADrMvsKnV0L7d6xS2bHPuV9NBsM9JXYDo_v6S2lcQ4"

// try {
//     let infoObj = jwt.verify(token,"dw11")
//     console.log(infoObj)
// } catch (error) {
//     console.log(error.message)
// }

//for token to be validate
//token must be made from the given secret key
//token must  not expire


// if token is valid it given infoObj else error