import { Schema } from "mongoose";

export let contactSchema =Schema({
    fullName:{
        type:String,
        required:true,
   },
    address:{
        type:String,
        required:true,
    },
    phone:{
        type:Number,
        required:true,
    },
    email:{
        type:String,
        required:true,

    },
})