import { Product } from "../schema/model.js"

export let createProduct=async(req,res,next)=>{
    let data=req.body
    try {
        let result = await Product.create(data)
        res.json({
            success:true,
            message:"product create successfully",
            result:result,
        })
    } catch (error) {
        res.json({
            success:false,
                message:error.message,
            })
    }
    
   
}

export let readProduct=async(req,res,next)=>{
    
    try {
        
let result = await Product.find({})
res.json({
    success:true,
    message:("product read successfully"),
    result:result,
})
    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
    }
}

export let readSpecificProduct =async (req,res,next)=>{
let id =req.params.id
try {
    let result =await Product.findById(id)
res.json({
    success:true,
    message:("product readspecific successfully"),
    result:result,
})
} catch (error) {
    res.json({
        
    success:false,
    message:error.message,
    })
}

}

export let updateProduct = async(req,res,next)=>{
    let id =req.params.id
    let data =req.body
    try {
        let result =await Product.findByIdAndUpdate(id,data,{new:true})
        res.json({
            success:true,
            message:("product update successfully"),
            result:result,
        })

    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
        
    }
}

export let deleteProduct = async(req,res,next)=>{
    let id = req.params.id
    try {
        let result = await Product.findByIdAndDelete(id)
        if (result===null) {
            res.json({
                success:false,
                message:"product does not exit"
            })
            
        } else {
            res.json({
                success:true,
                message:("product delete successfully"),
                result:result,
            }) 
        }
      

    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }

}