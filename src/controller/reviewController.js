import { Review } from "../schema/model.js"


export let createReview=async(req,res,next)=>{
    let data=req.body
    try {
        let result = await Review.create(data)
        res.json({
            success:true,
            message:"review create successfully",
            result:result,
        })
    } catch (error) {
        res.json({
            success:false,
                message:error.message,
            })
    }
    
   
}

export let readReview=async(req,res,next)=>{
    
    try {
        
let result = await Review.find({}).populate("productId").populate("userId")
res.json({
    success:true,
    message:("review read successfully"),
    result:result,
})
    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
    }
}

export let readSpecificReview =async (req,res,next)=>{
let id =req.params.id
try {
    let result =await Review.findById(id)
res.json({
    success:true,
    message:("review readspecific successfully"),
    result:result,
})
} catch (error) {
    res.json({
        
    success:false,
    message:error.message,
    })
}

}

export let updateReview = async(req,res,next)=>{
    let id =req.params.id
    let data =req.body
    try {
        let result =await Review.findByIdAndUpdate(id,data,{new:true})
        res.json({
            success:true,
            message:("review update successfully"),
            result:result,
        })

    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
        
    }
}

export let deleteReview = async(req,res,next)=>{
    let id = req.params.id
    try {
        let result = await Review.findByIdAndDelete(id)
        if (result===null) {
            res.json({
                success:false,
                message:"review does not exit"
            })
            
        } else {
            res.json({
                success:true,
                message:("review delete successfully"),
                result:result,
            }) 
        }
      

    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }

}