import { Router } from "express";
import { Description } from "../schema/model.js";


export let descriptionRouter = Router()


descriptionRouter
.route("/")
.post(async(req,res,next)=>{
   let data=req.body

    try {
        let result =await Description.create(data)
        console.log(result)
        res.json({
            success:true,
            message:"description create successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
    }

})


.get(async(req,res,next)=>{
try {
    let result =await Description.find({})
    res.json({
        success:true,
        message:"description read succssefully",
        result: result,
    })
    
} catch (error) {
    res.json({
        success:false,
        message:"unable to read description",
    })
}
})

descriptionRouter
.route("/:id")
.get(async(req,res,next)=>{
    let id=req.params.id
    console.log(id)
    try {
        let result =await Description.findById(id)
        res.json({
            success:true,
            message:"description read successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
    }

})
.patch(async(req,res,next)=>{
    let id= req.params.id
    let data = req.body
    try {
        let result=await Description.findByIdAndUpdate(id,data,{new:true})
        res.json({
            success:true,
            message:"description updated successfully.",
            result:result,
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message,
            
        })
        
    }

})