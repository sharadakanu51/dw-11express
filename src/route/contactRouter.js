import { Router } from "express";
import { createContact, deleteContact, readContact, readSpecificContact, updatedContact } from "../controller/contactController.js";

export let contactRouter = Router()

contactRouter
.route("/")
.post(createContact)
.get(readContact)

contactRouter
.route("/:id")
.get(readSpecificContact)
.patch(updatedContact)
.delete(deleteContact)
