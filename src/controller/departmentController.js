import { Department } from "../schema/model.js"


export let createDepartment =async(req,res,next)=>{
    let data= req.body
    try {
       let result = await Department.create(data) 
       res.json({
        success:true,
        message:("deparment create successfully"),
        result:result,
       })
    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
    }

}

export let getDepartment =async (req,res,next)=>{
try {
    let result = await Department.find({})
    res.json({
        success:true,
        message:("department read successfully"),
        result:result,
    })
} catch ( error) {
    res.json({
        success:false,
        message: error.message,
    })
}
}

export let getSpecificDepartment=async(req,res,next)=>{
    let id =req.params.id
    console.log(id)
    try {
        let result =await Department.findById(id)
        res.json({
            success:true,
            message:"department read successfully",
            result:result,
        }) 
    } catch (error) {
        res.json({
            success:false,
            message:error.message,
            
        })
    }

}

export let updateDepartment=async(req,res,next)=>{
    let id= req.params.id
    let data = req.body
    try {
        let result=await Department.findByIdAndUpdate(id,data,{new:true})
        res.json({
            success:true,
            message:"department  updated sucessfully",
            result:result,
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message,
            
        })
        
    }

}

export let deleteDepartment = async(req,res,next)=>{
    let id =req.params.id
    try {
        let result = await Department.findByIdAndDelete(id)
        res.json({
            success:true,
            message:("department delete successfully"),
            result:result,
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
    }

}