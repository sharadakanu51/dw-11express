import { Schema } from "mongoose";


export let reviewSchema = Schema({
    productId:{
        type:Schema.ObjectId,//(postmen ma use garne schema)
        ref:"Product", //ref ho (model ko product liyko)
        required:true, 
    },
    userId:{
        type:Schema.ObjectId,
        ref:"User",
        required:true,

    },
    description:{
        type:String,
        required:true,
    },
})
