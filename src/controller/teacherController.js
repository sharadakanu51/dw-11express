import { Teacher } from "../schema/model.js"

export let createTeacher =async(req,res,next)=>{
    console.log("******")
    let data = req.body
    try {
        let result = await Teacher.create(data)
        res.json({
            success:true,
            message:"teacher create successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
                message:error.message,
            })
    }


}

export let readTeacher =async (req,res,next)=>{
    
try {
    
    let result =await Teacher.find({})
 res.json({
    success:true,
    message:"teacher read successfully",
    result:result
 })
} catch (error) {
    res.json({
        success:false,
        message:error.message
    })
}
}

export let readSpecificTeacher =async(req,res,next)=>{
    let id = req.params.id
    try {
        let result= await Teacher.findById(id)
        res.json({
            success:true,
            message:"all teacher read successfully",
            result:result

        })

    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }
}

export let updatedTeacher =async(req,res,next)=>{
let id = req.params.id
let data =req.body
try {
    let result= await Teacher.findByIdAndUpdate(id,data,{new:true})
    res.json({
        success:true,
        message:"teacher updated successfully",
        result:result,
    })
} catch (error) {
    res.json({
        success:false,
        message:error.message
    })
    
}

}


export let deleteTeacher=async(req,res,next)=>{
    let id =req.params.id
    try {
        let result=await Teacher.findByIdAndDelete(id)
        if(result===null){
            res.json({
                success:false,
                message:"teacher does not exist"
            })

        }
        else{
            res.json({
                success:true,
                message:"teacher  deleted successfully",
                result:result,
            }) 
        }
        

    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
    
    }}