import { config } from "dotenv";


config()

export let port = process.env.PORT
export let mongoUrl =process.env.MONGO_URL
export let secretKey = process.env.SECRET_KEY
export let User = process.env.User
export let Pass = process.env.Pass

