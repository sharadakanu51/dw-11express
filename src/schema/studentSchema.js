import { Schema } from "mongoose";


export let studentSchema = Schema({
    name:{
        type:String,
        required:[true,"name field is required"],
        // lowercase:true, // manipulation
        // uppercase:true,//manipulation
        // trim:true,//manipulation
        minLength:[3,"name must be at least 3 character long"],//validation
        maxLength:[30,"name must be at least 30 character long"],//validation
        validate:(value)=>{
            //regex = pattern
            let onlyAlphabet = /^[A-Za-z]+$/.test(value)
            if (onlyAlphabet){

            }else{
                throw new Error("name filed must have only alphabet")
            }
        },
    },
   gender:{
    type:String,
    default:"male",//manipulation
    required:[true,"gender field is required"],
    validate:(value)=>{
if (value ==="male" || value==="female"|| value==="other"){

}else{
    let error = new Error("gender must be either male,female,other")
}
    },
   },
   roll:{
    type:Number,
    required:[true,"roll field is required"],
    min:[50,"roll must be greater than or equal to 50 "],//inbuilt validation
    max:[100,"roll must be less than or equal to 100"],//inbuilt validation
   },
   phoneNumber:{
    type:Number,
    required:[true,"phoneNumber field is required"],
    trim:true,
    validate:(value)=>{
        let strPhoneNumber = String(value)
        if (strPhoneNumber.length===10){

        }else{
            throw new Error("phoneNumber must be exact 10 character long")
        }
    },
   },
    
    password:{
        type:String,
        required:[true,"password field is required"],
       //   password must have 1 number, one lower case, one uppercase , one symbol, min 8 character, max 15 character
       validate:(value)=>{
        let isValidPassword=/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_+,\-./:;<=>?@[\\\]^_`{|}~])[a-zA-Z0-9!@#$%^&*()_+,\-./:;<=>?@[\\\]^_`{|}~]{8,15}$/.test(value)
        if(isValidPassword){

        }
        else{
            throw new Error("  password must have 1 number, one lower case, one uppercase , one symbol, min 8 character, max 15 character")
        }
       },
    },
    isMarried:{
        type:Boolean,
        required:[true,"isMarried field is required"]
    },
    spouseName:{
        type:String,
        required:[true,"spouseName field is required"]
    },
    email:{
        type:String,
        required:[true,"email field is required"],
         validate:(value)=>{
            let isvalidEmail=/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(value)
            if(isvalidEmail){

            }
            else{
                throw new Error("email must be valid")
            }
        },
        unique:true,
    },
    dob:{
      type:Date,
     required:[true,"dob field is required"]
    },
    location:{
        country:{
            type:String,
            required:[true,"country field is required"]
        },
        exactLocation:{
            type:String,
            required:[true,"exactlocation field is required"]
        }
    },
    favTeacher:[
        {
            type:String,
        }
    ],
    favSubject:[
        {
            bookName:{
                type:String,
            },
            bookAuthor:{
                type:String,
            }
        },
    ],
  

})



