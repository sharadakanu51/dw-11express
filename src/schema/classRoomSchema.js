import { Schema } from "mongoose";


export let classRoomSchema= Schema({
    name:{
        type:String,
        required:true,
    },
    NumberofBench:{
        type:Number,
        required:true,
    },
    hasTv:{
        type:Boolean,
        required:true,
    },
})