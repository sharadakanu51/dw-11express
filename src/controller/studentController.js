import { Student } from "../schema/model.js"

export let createStudent=async(req,res,next)=>{
    
    let data=req.body
    console.log(data)
    try {
        let result = await Student.create(data)
        res.json({
            success:true,
            message:"student create successfully",
            result:result
        })
    } catch (error) {
        res.json({
            success:false,
                message:error.message,
                
            })
    }
    
   
}

export let getStudent=async(req,res,next)=>{
    try {
        // let result = await Student.find({}) 
        // let result = await Student.find({name:"samikchhaya"}) 
        // let result = await Student.find({name:"samikchhaya",roll:55}) 
        //while searching we only foucs on value (we do not focus on type)

        //*******number searching

        // let result = await Student.find({roll:50}) // roll=>50 lai matra 
        // let result = await Student.find({roll:{$gt:50}}) //greater than 50 vane ko ho
        // let result = await Student.find({roll:{$gte:50}}) // greater than equal to 50 vaneko ho
        // let result = await Student.find({roll:{$lt:50}}) //less than 50
        // let result = await Student.find({roll:{$lte:50}}) //less than equal to 50
        // let result = await Student.find({roll:{$ne:50}}) // not equal to 50
        // let result = await Student.find({roll:{$in:[20,25,30]}}) //include 20,25,30
        // let result = await Student.find({roll:{$gte:20,$lte:25}})//

        //*****string searching

        // let result = await Student.find({name:"sharada"})  
        // let result = await Student.find({name:{$in:["sharada","ram"]}})

        //******regex searching = not exact searching

// let result=await Student.find({name:"nitan"})
// let result=await Student.find({name:/nitan/})
// let result=await Student.find({name:/nitan/i})
// let result=await Student.find({name:/ni/})
// let result=await Student.find({name:/ni/i})
// let result=await Student.find({name:/^ni/})
//let result=await Student.find({name:/^ni/i})
// let result=await Student.find({name:/ni$/})

//*****select
//find has control over the object where as select as control over the object properties
// let result = await Student.find({}).select("name gender -_id")
// let result = await Student.find({}).select("-name -gender")//(-garer lekheyo vaneko name ra gender naaaune output)
// let result = await Student.find({}).select("name -gender")//+ -lekhan mildainn(it is not valid)

//in select use either all - ....or use all + but do not use both except -_id(-_id lekhan milx tra )

//****sorting(asending desending(-) bat garne ho)

//let result = await Student.find({}).sort("name")
//let result = await Student.find({}).sort("-name")
//let result = await Student.find({}).sort("name age")
//let result = await Student.find({}).sort("name -age")
//let result = await Student.find({}).sort("-name age")
//let result = await Student.find({}).sort("age -name")

//****skip(hataunu 3 ota starting bat hataune )

//let result = await Student.find({}).skip("3")

//***** limit
//let result = await Student.find({}).limit("3")

//this order works
//find ,sort,select,skip,limit
//   let result =await Student.find({}).skip("2").limit("5")



        res.json({
            success:true,
            message:"student read successfully",
            result:result,
        })
        
    } catch (error) {
        res.json({
            success:false,
            message:"Unable to read student",
        })
        
    }
       

    
    
   

}

export let getSpecificStudent=async(req,res,next)=>{
    let id =req.params.id
    console.log(id)
    try {
        let result =await Student.findById(id)
        res.json({
            success:true,
            message:"student read successfully",
            result:result,
        }) 
    } catch (error) {
        res.json({
            success:false,
            message:error.message,
            
        })
    }

}

export let updatedStudent=async(req,res,next)=>{
    let id= req.params.id
    let data = req.body
    try {
        let result=await Student.findByIdAndUpdate(id,data,{new:true})
        // if {new:false}it give old data
        //if {new:true} it give new data
        res.json({
            success:true,
            message:"student  updated sucessfully",
            result:result,
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message,
            
        })
        
    }

}

export let deleteStudent=async(req,res,next)=>{
    let id =req.params.id
    try {
        let result=await Student.findByIdAndDelete(id)
        if(result===null){
            res.json({
                success:false,
                message:"student does not exist"
            })

        }
        else{
            res.json({
                success:true,
                message:"student deleted successfully",
                result:result,
            }) 
        }
        

    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
    }

   

}

