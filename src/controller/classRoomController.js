
import { ClassRoom,  } from "../schema/model.js"

export let createClassRoom=async(req,res,next)=>{
    let data=req.body
    try {
        let result=await ClassRoom.create(data)
        res.json({
            success:true,
            message:"classRoom create successfully",
            result:result,
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
    }
}

export let readClassRoom =async(req,res,next)=>{
    try {
        let result=await ClassRoom.find({})
        res.json({
            success:true,
            message:"classRoom read successfully",
            result:result,

        })
    } catch (error) {
        res.json({
            success:false,
            message:"unable to read successfully",
            
        })
        
    }

}

export let readSpecificClassRoom=async(req,res,next)=>{
    let id=req.params.id
    console.log(id)
    try {
        let result=await ClassRoom.findById(id)
        res.json({
            success:true,
            message:"classRoom read successfully",
            result:result,
        })
    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
        
    }
}

export let updateClassRoom=async(req,res,next)=>{
    let id =req.params.id
    let data=req.body
    try {
        let result = await ClassRoom.findByIdAndUpdate(id,data,{new:true})
        res.json({
            success:true,
            message:"ClassRoom update successfully",
            result:result,
        })
        

    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
        
    }

}

export let deleteClassRoom=async(req,res,next)=>{
    let id =req.params.id
    try {
        let result=await ClassRoom.findByIdAndDelete(id)
        if(result===null){
            res.json({
                success:false,
                message:"classroom does not exist"
            })

        }
        else{
            res.json({
                success:true,
                message:"classroom  deleted successfully",
                result:result,
            }) 
        }
        

    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
    }

   

}
