import { Book } from "../schema/model.js"

export let createBook=async(req,res,next)=>{
    let data=req.body
    try {
        let result = await Book.create(data)
        res.json({
            success:true,
            message:"book create successfully",
            result:result,
        })
    } catch (error) {
        res.json({
            success:false,
                message:error.message,
            })
    }
    
   
}

export let readBook=async(req,res,next)=>{
    
    try {
        
let result = await Book.find({})
res.json({
    success:true,
    message:("book read successfully"),
    result:result,
})
    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
    }
}

export let readSpecificBook =async (req,res,next)=>{
let id =req.params.id
try {
    let result =await Book.findById(id)
res.json({
    success:true,
    message:("book readspecific successfully"),
    result:result,
})
} catch (error) {
    res.json({
        
    success:false,
    message:error.message,
    })
}

}

export let updateBook = async(req,res,next)=>{
    let id =req.params.id
    let data =req.body
    try {
        let result =await Book.findByIdAndUpdate(id,data,{new:true})
        res.json({
            success:true,
            message:("book update successfully"),
            result:result,
        })

    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
        
    }
}

export let deleteBook = async(req,res,next)=>{
    let id = req.params.id
    try {
        let result = await Book.findByIdAndDelete(id)
        if (result===null) {
            res.json({
                success:false,
                message:"book does not exit"
            })
            
        } else {
            res.json({
                success:true,
                message:("book delete successfully"),
                result:result,
            }) 
        }
      

    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }

}