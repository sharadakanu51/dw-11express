import { Hostel } from "../schema/model.js"


export let createHostel=async(req,res,next)=>{
    let data=req.body
    // console.log(data)
    try {
        let result = await Hostel.create(data)
        res.json({
            success:true,
            message:"hostel create successfully",
            result:result,
        })
    } catch (error) {
        res.json({
            success:false,
                message:error.message,
            })
    }
    
   
}

export let readHostel=async(req,res,next)=>{
    
    try {
        
let result = await Hostel.find({})
res.json({
    success:true,
    message:("hostel read successfully"),
    result:result,
})
    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
    }
}

export let readSpecificHostel =async (req,res,next)=>{
let id =req.params.id
try {
    let result =await Hostel.findById(id)
res.json({
    success:true,
    message:("hostel readspecific successfully"),
    result:result,
})
} catch (error) {
    res.json({
        
    success:false,
    message:error.message,
    })
}

}

export let updateHostel = async(req,res,next)=>{
    let id =req.params.id
    let data =req.body
    try {
        let result =await Hostel.findByIdAndUpdate(id,data,{new:true})
        res.json({
            success:true,
            message:("hostel update successfully"),
            result:result,
        })

    } catch (error) {
        res.json({
            success:false,
            message:error.message,
        })
        
    }
}

export let deleteHostel = async(req,res,next)=>{
    let id = req.params.id
    try {
        let result = await Hostel.findByIdAndDelete(id)
        if (result===null) {
            res.json({
                success:false,
                message:"hostel does not exit"
            })
            
        } else {
            res.json({
                success:true,
                message:("hostel delete successfully"),
                result:result,
            }) 
        }
      

    } catch (error) {
        res.json({
            success:false,
            message:error.message
        })
    }

}