import { Router } from "express";
import { createStudent, deleteStudent, getSpecificStudent, getStudent, updatedStudent } from "../controller/studentController.js";



export let studentRouter = Router()

studentRouter
.route("/")
.post(createStudent)
.get(getStudent)

studentRouter
.route("/:id")
.get(getSpecificStudent)
.patch(updatedStudent)
.delete(deleteStudent)





// url = localhost:8000/students  , method = post     res => {success:true, message:"Student create successfully"}
// url = localhost:8000/students  , method = get     res => {success:true, message:"Student Read successfully"}
// rl = localhost:8000/students/any  , method = get     res => { message:"student cerate successfully", message:"Student Read successfully"}
// rl = localhost:8000/students/any , method = patch     res => {success:true, message:"Student Updated successfully"}
// rl = localhost:8000/students/any , method = delete     res => {success:true, message:"Student deleted successfully"}