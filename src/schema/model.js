//defined array is called model
//name
//object

import { model } from "mongoose";
import { studentSchema } from "./studentSchema.js";
import { traineeSchema } from "./traineeSchema.js";
import { collegeSchema } from "./collegeSchema.js";
import { departmentSchema } from "./departmentSchema.js";
import { descriptionSchema } from "./descriptionSchema.js";
import { contactSchema } from "./contactSchema.js";
import { classRoomSchema } from "./classRoomSchema.js";
import { hostelSchema } from "./hostelSchema.js";
import { teacherSchema } from "./teacherSchema.js";
import { bookSchema } from "./bookSchema.js";

import { productSchema } from "./productSchema.js";
import { userSchema } from "./userSchema.js";
import { reviewSchema } from "./reviewSchema.js";





//model name must be singular

export let Student= model("Student",studentSchema)



export let Trainee =model("Trainee",traineeSchema)

export let College = model("College",collegeSchema)


export let Department =model("Department",departmentSchema)
export let Description=model("Description",descriptionSchema)
export let Contact =model("Contact",contactSchema)
export let ClassRoom = model("ClassRoom",classRoomSchema)
export let Hostel = model("Hostel",hostelSchema)
export let Teacher = model("Teacher",teacherSchema)

export let Book =model("Book",bookSchema)
export let Product = model("Product",productSchema)
export let User = model("User",userSchema)
export let Review = model("Review",reviewSchema)

