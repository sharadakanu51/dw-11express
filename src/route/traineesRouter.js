import { Router } from "express";

export let traineesRouter = Router()

traineesRouter
.route("/a")//localhost:8000/trainees
.post((req,res,next)=>{
    console.log("i am sharada")
    console.log(req.body)
})


traineesRouter
.route("/:id/a/:name/b/:address")//localhost:8000/trainees/any/a/name/b/address
.get((req,res,next)=>{
    console.log(req.params)
    res.json({
        success:true,
    })

})


traineesRouter
.route("/:id/a/:name")//localhost:8000/trainees/any/a/any
.post((req,res,next)=>{
    console.log(req.body)
    console.log(req.params)
    console.log(req.query)
    res.json({
        success:true,
    })
})

// dynamic route param
//url =route?query
// url=localhost:8000/


// url=localhost:8000/trainees,post at response {success:true, message:"trainees created successfully"}
// url=localhost:8000/trainees,get at response {success:true, message:"trainees read successfully"}
// url=localhost:8000/trainees,patch at response {success:true, message:"trainees updated successfully"}
// url=localhost:8000/trainees,delete at response {success:true, message:"trainees deleted successfully"}

