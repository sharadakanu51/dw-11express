import { Schema } from "mongoose";

export let teacherSchema = Schema({
    name:{
        type:String,
        required:true,
    },
    age:{
        type:Number,
        required:true,
    },
    isMarried:{
        type:Boolean,
        required:true,

    },
    subject:{
        type:String,
        required:true,
    },
})